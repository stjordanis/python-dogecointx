from .version import __version__

import dogecointx.core
import dogecointx.core.script
import dogecointx.wallet

from bitcointx import ChainParamsBase


# Declare chain params after frontend classes are regstered in wallet.py,
# so that issubclass checks in ChainParamsMeta.__new__() would pass
class DogecoinMainnetParams(ChainParamsBase,
                            name=('dogecoin', 'dogecoin/mainnet')):
    RPC_PORT = 22555
    WALLET_DISPATCHER = dogecointx.wallet.WalletDogecoinClassDispatcher

    def __init__(self, allow_legacy_p2sh: bool = False) -> None:
        super().__init__()
        self.allow_legacy_p2sh = allow_legacy_p2sh


class DogecoinTestnetParams(DogecoinMainnetParams, name='dogecoin/testnet'):
    RPC_PORT = 44555
    WALLET_DISPATCHER = dogecointx.wallet.WalletDogecoinTestnetClassDispatcher

    def get_datadir_extra_name(self) -> str:
        return 'testnet3'

    def get_network_id(self) -> str:
        return "test"


class DogecoinRegtestParams(DogecoinMainnetParams, name='dogecoin/regtest'):
    RPC_PORT = 18332
    WALLET_DISPATCHER = dogecointx.wallet.WalletDogecoinRegtestClassDispatcher


__all__ = (
    '__version__',
    'DogecoinMainnetParams',
    'DogecoinTestnetParams',
    'DogecoinRegtestParams'
)
