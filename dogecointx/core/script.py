from bitcointx.core.script import (
    CScript, ScriptCoinClassDispatcher, ScriptCoinClass
)


class ScriptDogecoinClassDispatcher(ScriptCoinClassDispatcher):
    ...


class ScriptDogecoinClass(ScriptCoinClass,
                          metaclass=ScriptDogecoinClassDispatcher):
    ...


class CDogecoinScript(CScript, ScriptDogecoinClass):
    ...


__all__ = (
    'CDogecoinScript',
)
